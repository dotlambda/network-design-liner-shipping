PDFLATEX = lualatex -synctex=1 -interaction=nonstopmode
LATEXMKOPT = -bibtex -pdf -pdflatex="$(PDFLATEX)"

PDF_TEX = $(shell for file in *.svg; do echo $${file%.svg}.pdf_tex; done)

.PHONY: slides.pdf report.pdf all

all: $(PDF_TEX) traffic.csv slides.pdf report.pdf

clean:
	latexmk -c $(LATEXMKOPT); \
	rm -f $(PDF_TEX); \
	rm -f traffic.csv; \
	for e in $(PDF_TEX); do rm -f $${e%_tex}; done

cleanall: clean
	latexmk -C $(LATEXMKOPT)

slides.pdf: slides.tex
	latexmk $(LATEXMKOPT) $<

report.pdf: report.tex
	latexmk $(LATEXMKOPT) $<

%.pdf_tex: %.svg
	file=$@; inkscape -D -z --file=$+ --export-pdf=$${file%_tex} --export-latex

traffic.csv:
	wget "http://api.worldbank.org/v2/en/indicator/IS.SHP.GOOD.TU?downloadformat=csv" -O tmp.zip; \
	unzip tmp.zip API_IS.SHP.GOOD.TU_DS2_en_csv_v2.csv; \
	./traffic.py; \
	rm tmp.zip API_IS.SHP.GOOD.TU_DS2_en_csv_v2.csv
