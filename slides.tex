\documentclass[10pt,fleqn,aspectratio=169]{beamer}

\usetheme[
  titleformat=smallcaps,
  subsectionpage=progressbar,
  ]{metropolis}

\usepackage{mathtools}
\DeclarePairedDelimiter{\ceil}{\lceil}{\rceil}
\DeclareMathOperator{\Cost}{Cost}
\DeclareMathOperator{\In}{In}
\DeclareMathOperator{\Out}{Out}
\DeclareMathOperator{\argmax}{argmax}

\usepackage{algpseudocode,algorithm}

\title{Network Design for Liner Shipping}
\subtitle{Column generation and Benders decomposition}
\date{January 30, 2017}
\author{Robert Schütz}
\institute{Universität Heidelberg}

\begin{document}

\maketitle

\begin{frame}{Table of contents}
  \setbeamertemplate{section in toc}[sections numbered]
  \setbeamertemplate{subsection in toc}[square]
  \tableofcontents
\end{frame}

\section{Problem description}

\begin{frame}{Motivation I}
  \begin{figure}
    \centering{
      \resizebox{75mm}{!}{\input{graph.pdf_tex}}
      \caption{Growth in container movements worldwide \cite{Agarwal2008}
      (TEU = standard unit of containers)}
    }
  \end{figure}
\end{frame}

\begin{frame}{Motivation II}
  Previous algorithms did not account for
  \begin{itemize}
    \item planning cargo \emph{and} ship routes,
    \item a heterogeneous fleet,
    \item transshipment.
  \end{itemize}
\end{frame}

\begin{frame}{Goal}
  A carrier needs to plan a network for \emph{routing ships} and sea \emph{cargo}.
  
  The goal is to \alert{maximize revenue} w.r.t.\ the \alert{given demand and fleet}.
  
  The ships will operate different routes (\emph{cycles}).
  On each cycle, a \alert{weekly schedule} is to be maintained.
  
  We want to allow for \alert{transshipment},
  i.e.\ transferring cargo from one ship to another.
\end{frame}

\newcommand{\ports}{\mathcal{P}}
\newcommand{\days}{\{1,\dots,7\}}
\newcommand{\ships}{\mathcal{A}}

\begin{frame}{Given data}
  \begin{itemize}
    \item ports $\ports$
    \item demand (n° of containers)
      to ship from origin $o$ to destination $d$ occuring on weekday $i$:
      \[D^{o,d,i},\quad (o,d,i)\in\Theta\subseteq\{(o,d,i):o\neq d\in\ports,i\in\days\}\]
    \item Revenue for shipping one of these containers: $R^{o,d,i}$
    \item ship types $\ships$ with capacity $T^a$ of type $a\in\ships$
    \item travelling time (in days) from port $p$ to $q$:
      \[l_{p,q}^a,\quad p\neq q\in\ports,a\in\ships\]
    \item $N^a$ ships of type $a$ in the fleet
  \end{itemize}
\end{frame}

\begin{frame}{Network construction}
  \emph{Space-time} network $G=(V,E)$ with nodes $V=\{v_{p,i}:p\in\ports,i\in\days\}$.
  The edges $E=E_v\cup E_g\cup E_f$ with $E_v=\bigcup_{a\in\ships}E_v^a$ are split into
  \begin{itemize}
    \item voyage edges $E_v^a$ for $a\in\ships$ and $p\neq q\in\ports$:
      \[(v_{p,i},v_{q,j})^a\quad\text{such that}\quad i-j\equiv l_{p,q}^a\mod7\]
    \item ground edges $E_g$ for $p\in\ports$ and $i\in\{1,\dots,6\}$:
      \[(v_{p,i},v_{p,i+1})\quad\text{and}\quad(v_{p,7},v_{p,1})\]
    \item fictitious edges $E_f$ from destination to origin -- allow cargo flow to follow a cycle:
      \[(v_{d,j},v_{o,i})\quad\text{for }(o,d,i)\in\Theta\text{ and }j\in\days\]
  \end{itemize}
\end{frame}

\newcommand{\cycles}{\mathcal{C}}

\begin{frame}{Cycles}
  We construct the set $\cycles=\bigcup_{a\in\ships}\cycles^a$ of feasible cycles.
  For a ship type $a\in\mathcal{A}$:
  \begin{enumerate}
    \item Let $C=v_1-v_2-\dots-v_n-v_1$,\quad$v_1,\dots,v_n\in E_v^a\cup E_g$.
    \item $C\in\cycles^a$ is feasible if there are enough ships to maintain weekly frequency:
      \[L_C:=\ceil[\Big]{\sum_{e\in C}l_e^a/7}\leq N^a\]
    \item Let $\Cost_C=\sum_{v\in C}c_v^a+\sum_{e\in C}c_e^a$ be the costs of operating the cycle including
      \begin{itemize}
        \item port fees $c_v^a$,
        \item ship operating costs $c_e^a$.
      \end{itemize}
      We calculate these from the ship type and the selected edges.
  \end{enumerate}

  Costs for transporting a single container are represented by $c_e$, $e\in E_v\cup E_g$.
\end{frame}

\begin{frame}{Example network}
  \begin{figure}
    \centering{
      \resizebox{85mm}{!}{\input{network.pdf_tex}}
      \caption{Network with four ports and two cycles \cite{Agarwal2008}}
    }
  \end{figure}
\end{frame}

\begin{frame}{Variables}
  \begin{itemize}
    \item \alert{Binary} variables $x_C$ for every feasible cycyle $C$.
    \item Flow of commodity $(o,d,i)\in\Theta$ on edge $e\in E$: $f_e^{o,d,i}$
      \begin{itemize}
        \item On a fictitous edge $e=(v_{d,j},v_{o,i})\in E_f$,
          only $f_e^{o,d,i}$ is defined.
        \item These are nonnegative \alert{continuous} variables,
          even though representing an (integral) number of containers.
          
          This is of no relevance,
          since the number of containers is generally very high.
      \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}[label=ssscr]{Simultaneous ship-scheduling and cargo-routing problem}
  \only<1>{We get a \alert{mixed integer} linear program (MILP):}
  \begin{align*}
    &\text{(SSSCR):}
    &\max\sum_{(o,d,i)\in\Theta}\sum_{j=1}^7R^{o,d,i}f_{(v_{d,j},v_{o,i})}^{o,d,i}
    -\sum_{(o,d,i)\in\Theta}\sum_{e\in E}c_ef_e^{o,d,i}
    -\sum_{a\in\ships}\sum_{C\in\cycles^a}\Cost_Cx_C
    \end{align*}
    \begin{align*}
    &\text{such that}
    &\sum_{e\in\In(v)}f_e^{o,d,i}-\sum_{e\in\Out(v)}f_e^{o,d,i}
    &=0
    &\forall v\in V,\forall(o,d,i)\in\Theta \\
    &&\sum_{(o,d,i)\in\Theta}f_e^{o,d,i}-\sum_{a\in\ships}\sum_{\{C\in\cycles^a:e\in C\}}T^ax_C
    &\leq0
    &\forall e\in E_v \\
    &&\sum_{j=1}^7f_{(v_{d,j},v_{o,i})}^{o,d,i}
    &\leq D^{o,d,i}
    &\forall(o,d,i)\in\Theta \\
    &&\sum_{C\in\cycles^a}L_Cx_C
    &\leq N^a
    &\forall a\in\ships \\
    &&x_C
    &\in\{0,1\}
    &\forall C\in\cycles^a,\forall a\in\ships \\
    &&f_e^{o,d,i}
    &\geq0
    &\forall e\in E,\forall(o,d,i)\in\Theta
  \end{align*}
\end{frame}

\begin{frame}<1>[label=hardness]{Hardness}
  \begin{theorem}
    SSSCR is NP-complete.
  \end{theorem}
  \only<2>{
    \begin{proof}
      See blackboard.
    \end{proof}
  }
\end{frame}


\metroset{sectionpage=none}
\section{Techniques}
\metroset{sectionpage=progressbar}

\subsection{Column generation}

\begin{frame}{Original problem}
  We are given the original problem (OP) with a \alert{large number of columns} and compute the dual problem (DP).
  
  \begin{minipage}[t]{.3\textwidth}
    \begin{align*}
        &\text{(OP):}&\max\enskip&c^\intercal x \\
        &&\text{s.t.}\enskip&Ax\leq b \\
        &&&x\geq0
    \end{align*}
  \end{minipage}%
  \begin{minipage}[t]{.3\textwidth}
    \begin{align*}
      &\text{(DP):}&\min\enskip&b^\intercal p \\
      &&\text{s.t.}\enskip&A^\intercal p\geq c \\
      &&&p\geq0
    \end{align*}
  \end{minipage}
\end{frame}

\begin{frame}{Master \& Pricing problem}
  We start with a set $\mathcal{S}$ of columns and solve the \emph{master problem} (MP).
  The optimal solution $x^*$ of (MP) yields a dual point $p^*$ that is infeasible for (DP).
  
  The \emph{pricing problem} (PP) is to generate the column with maximal reduced costs at $x^*$,
  which means working towards dual feasibility.
  This column is added to $\mathcal{S}$.
  
  If (PP) can be reduced to a simpler problem, column generation may be good approach.
  
  \begin{minipage}{.3\textwidth}
    \begin{align*}
      &\text{(MP):}&\max\enskip&c_\mathcal{S}^\intercal x_\mathcal{S} \\
      &&\text{s.t.}\enskip&A_\mathcal{S}x_\mathcal{S}\leq b_\mathcal{S} \\
      &&&x_\mathcal{S}\geq0
    \end{align*}
  \end{minipage}%
  \begin{minipage}{.3\textwidth}
    \begin{align*}
      \text{(PP):}\enskip\argmax_{i\not\in\mathcal{S}}c_i-A_i^\intercal p^* \\\\
    \end{align*}
  \end{minipage}%
  \begin{minipage}{.3\textwidth}
    \begin{figure}
      \centering{
        \resizebox{55mm}{!}{\input{pricing_problem.pdf_tex}}
        \caption{Feasible sets of (OP) and (DP)}
      }
    \end{figure}
  \end{minipage}
\end{frame}

\subsection{Benders decomposition}

\begin{frame}[label=original]{Original problem}
  We are given the original problem (OP) and consider its LP relaxation (LP) for fixed feasible point $\bar{y}$.
  \uncover<3->{Let (DLP) be the dual problem of (LP).}
  
  \begin{minipage}{.2\textwidth}
    \begin{align*}
      &\text{(OP):}&\max\enskip&c^\intercal x+d^\intercal y \\
      &&\text{s.t.}\enskip&Ax+By=b \\
      &&&x\geq0 \\
      &&&y_i\in\{0,1\}
    \end{align*}
  \end{minipage}%
  \begin{minipage}{.2\textwidth}
    \begin{align*}
      &\text{(LP):}&\max\enskip&c^\intercal x\uncover<1>{+d^\intercal\bar{y}} \\
      &&\text{s.t.}\enskip&Ax=b-B\bar{y} \\
      &&&x\geq0
    \end{align*}
    \hfill
  \end{minipage}%
  \begin{minipage}{.4\textwidth}
    \uncover<3->{\begin{align*}
      &\text{(DLP):}&\min\enskip&(b-B\bar{y})^\intercal p \\
      &&\text{s.t.}\enskip&A^\intercal p\geq c \\
      &&&p\text{ free}
    \end{align*}}
    \hfill
  \end{minipage}

  \uncover<4->{
    The feasible region $D$ of (DLP) does not depend on $\bar{y}$.
    
    Since $\bar{y}$ is feasible, (DLP) is bounded.
    It can be expressed in terms of the extreme points $P_D$ and extreme rays $R_D$ of $D$:
    \begin{align*}
      \min_{p\in P_D}\enskip&(b-B\bar{y})^\intercal p\\
      \text{s.t.}\enskip&(b-B\bar{y})^\intercal r\geq0\quad\forall r\in R_D
    \end{align*}
  }
\end{frame}

\begin{frame}<3>{Master problem}
  \begin{minipage}[t]{.6\textwidth}
    (OP) can be rewritten as
    \begin{align*}
      &\text{(MP):}&\max\enskip&z \\
      &&\text{s.t.}\enskip&z\leq(b-By)^\intercal p+d^\intercal y&\forall p\in P_D \\
      &&&(b-By)^\intercal r\geq0&\forall r\in R_D \\
      &&&y_i\in\{0,1\} \\
      &&&z\text{ free}
    \end{align*}
  \end{minipage}%
  \begin{minipage}[t]{.4\textwidth}
    The subproblem is the same as (LP):
    \begin{align*}
      &\text{(SP):}&\max\enskip&c^\intercal x \\
      &&\text{s.t.}\enskip&Ax=b-B\bar{y} \\
      &&&x\geq0
    \end{align*}
  \end{minipage}
  
  The constraint involving $P_D$ insures optimality.
  The constraint involving $R_D$ insures that (DP) is bounded and therfore that $y$ is feasible.
\end{frame}

\begin{frame}{Benders decomposition}
  \begin{minipage}{.55\textwidth}
    The idea behind Benders decomposition is,
    starting with $P_D=\emptyset$, $R_D=\emptyset$:
    \begin{enumerate}
      \item Optimal cost $\bar{z}$ of (MP) is an upper bound.
      \item Take $\bar{y}$ from this solution and compute (SP).
      \item If (SP) is bounded, $c^\intercal\bar{x}+d^\intercal\bar{y}$ is a lower bound.
        Dual solution $\bar{p}$ corresponding to $\bar{y}$ is added to $P_D$.
        (\emph{optimality cut})
      \item Otherwise, (MP) is infeasible.
        The extreme ray $\bar{r}$ corresponding to infinite reduced costs is added to $R_D$.
        (\emph{feasibility cut})
      \item Repeat until the optimality gap (upper bound $-$ lower bound) is small enough.
    \end{enumerate}
  \end{minipage}%
  \begin{minipage}{.45\textwidth}
    \begin{figure}
      \centering{
        \resizebox{60mm}{!}{\input{benders.pdf_tex}}
        \caption{Feasible sets of (MP) and (SP)}
      }
    \end{figure}
  \end{minipage}
\end{frame}

\section{Algorithms}

\againframe<2>{ssscr}

\begin{frame}{LP relaxation}
  For a predetermined set of selected cycles,
  i.e. fixed $\bar{x}_C$,
  (SSSCR) reduces to a \emph{multi-commodity flow} problem:
  
  \begin{align*}
    &\text{(MCF):}
    &\max\sum_{(o,d,i)\in\Theta}\sum_{j=1}^7R^{o,d,i}f_{(v_{d,j},v_{o,i})}^{o,d,i}
    -\sum_{(o,d,i)\in\Theta}\sum_{e\in E}c_ef_e^{o,d,i}
  \end{align*}
  \begin{align*}
    &\text{such that}
    &\sum_{e\in\In(v)}f_e^{o,d,i}-\sum_{e\in\Out(v)}f_e^{o,d,i}
    &=0
    &\forall v\in V,\forall(o,d,i)\in\Theta \\
    &&\sum_{(o,d,i)\in\Theta}f_e^{o,d,i}-\sum_{a\in\ships}\sum_{\{C\in\cycles^a:e\in C\}}T^a\bar{x}_C
    &\leq0
    &\forall e\in E_v \\
    &&\sum_{j=1}^7f_{(v_{d,j},v_{o,i})}^{o,d,i}
    &\leq D^{o,d,i}
    &\forall(o,d,i)\in\Theta \\
    &&f_e^{o,d,i}
    &\geq0
    &\forall e\in E,\forall(o,d,i)\in\Theta
  \end{align*}
\end{frame}

\begin{frame}{Dual of (MCF)}
  We introduce dual variables
  \begin{itemize}
    \item $\pi_v^{o,d,i}$,\quad$v\in V,(o,d,i)\in\Theta$
    \item $\lambda_e$,\quad$e\in E_v$
    \item $\omega^{o,d,i}$,\quad$(o,d,i)\in\Theta$
  \end{itemize}

  \begin{align*}
    &\text{(DP):}
    &\min\sum_{e\in E_v}\sum_{a\in\ships}\sum_{\{C\in\cycles^a:e\in C\}}T^a\bar{x}_C\lambda_e
    +\sum_{(o,d,i)\in\Theta}D^{o,d,i}\omega^{o,d,i}
  \end{align*}
  \begin{align*}
    &\text{such that}
    &\pi_u^{o,d,i}-\pi_v^{o,d,i}+\lambda_{(u,v)}
    &\geq-c_{(u,v)}
    &\forall(u,v)\in E\setminus E_f \\
    &&\pi_u^{o,d,i}-\pi_v^{o,d,i}
    &\geq R^{o,d,i}-c_e
    &\forall e\in E_f,\forall(o,d,i)\in\Theta \\
    &&\pi_v^{o,d,i}
    &\text{ free}
    &\forall v\in V,\forall(o,d,i)\in\Theta \\
    &&\lambda_e
    &\geq0
    &\forall e\in E_f \\
    &&\omega^{o,d,i}
    &\geq0
    &\forall(o,d,i)\in\Theta
  \end{align*}
\end{frame}

\subsection{Greedy algorithm}

\begin{frame}<1>[label=greedy]{Greedy algorithm}
  \begin{enumerate}
    \item Start with an empty set $\mathcal{S}$ of selected cycles.
    \item For each $a\in\ships$,
    construct an auxiliary network $G^a$ in which we search for minimum cost cycles.
    \item A feasible cycle $C$\only<2>{, i.e. enough ships are available,} with minimum cost is added to $\mathcal{S}$.
  \end{enumerate}
  \only<2>{
    \begin{align*}
      G^a&=(V,E^a)\quad\text{with }E^a=E_v^a\cup E_g^a \\
      c_e'&=c_e^a+\lambda_e \\
      c_v'&=c_v^a
    \end{align*}
  }
\end{frame}

\subsection{Column generation}

\begin{frame}{Column generation-based algorithm}
  We want to solve the LP relaxation of (SSSCR).
  \begin{enumerate}
    \item Start with an empty set $\mathcal{S}$ of selected cycles.
    \item Solve the LP relaxation of (SSSCR).
    \item The pricing problem reduces to finding negative cost cycles in networks $G^a,a\in\ships$:
      \begin{align*}
        G^a&=(V,E^a)\quad\text{with }E^a=E_v^a\cup E_g^a \\
        c_e'&=c_e^a+T^a\lambda_e+(l_e^a/7)\sigma^a \\
        c_v'&=c_v^a
      \end{align*}
    \item The cycle $C$ with maximum reduced costs is added to $\mathcal{S}$.
  \end{enumerate}
  Finally, we solve (SSSCR) -- restricted to the cycles $\mathcal{S}$ -- using branch-and-bound.
\end{frame}

\subsection{Benders decomposition}

\begin{frame}{Idea}
  The problem has many variables and many constraints.
  
  $\Rightarrow$ Use \emph{both} column generation and Benders decomposition.
\end{frame}

\begin{frame}[allowframebreaks]{Benders master problem}
  Let $D$ be the feasible set of (DP).
  $\pi_v^{o,d,i}=0$, $\lambda_e=0$, $\omega^{o,d,i}=R^{o,d,i}$ is a feasible dual solution.
  Since $0$ is a feasible solution for (MCF),
  both problems are feasible and bounded.
  Thus, we can characterize (DP) in terms of only the extreme points $P_D$ of $D$:
  \[\min_{(\pi,\lambda,\omega)\in P_D}\sum_{e\in E_v}\left(\sum_{a\in\ships}\sum_{\{C\in\cycles^a:e\in C\}}T^a\bar{x}_C\right)\lambda_e
  +\sum_{(o,d,i)\in\Theta}D^{o,d,i}\omega^{o,d,i}\]
  
  The Benders master problem then reads
  \begin{align*}
    &\text{(BMP):}
    &\max z \\
    &\text{such that}
    &z
    &\leq\sum_{e\in E_v}\left(\sum_{a\in\ships}\sum_{\{C\in\cycles^a:e\in C\}}T^ax_C\right)\lambda_e \\
    &&&\phantom{\leq}+\sum_{(o,d,i)\in\Theta}D^{o,d,i}\omega^{o,d,i}
    -\sum_{a\in\ships}\sum_{C\in\cycles^a}\Cost_Cx_C
    &\forall(\lambda,\omega)\in P_D \\
    &&\sum_{C\in\cycles^a}L_Cx_C
    &\leq N^a
    &\forall a\in\ships \\
    &&x_C
    &\in\{0,1\}
    &\forall C\in\cycles^a,\forall a\in\ships \\
    &&z
    &\text{ free}
  \end{align*}
  
  With restricted $P_D$, we call this (RBMP).
  When also droppping integer constraints (RLPBMP).
\end{frame}

\begin{frame}[allowframebreaks]{Benders decomposition-based algorithm}
  \begin{algorithm}[H]
    \caption{Basic Benders decomposition-based algorithm}
    \begin{algorithmic}[1]
      \State $P_D\gets\emptyset$
      \State $lower\_bound\gets0$
      \State $upper\_bound\gets\infty$
      \While{$upper\_bound>lower\_bound+\epsilon$}
        \State Solve (RLPBMP) to obtain $\bar{z}$ and $\bar{x}_C$.
        \State $upper\_bound\gets\bar{z}$
        \State Solve (MCF) with input $\bar{x}_C$.
        
          We obtain the primal optimum $v(\bar{x}_C)$
          and optimal dual solution $(\bar{\pi},\bar{\lambda},\bar{\omega})$.
        \State $lower\_bound\gets\max\{lower\_bound,v(\bar{x}_C)-\sum_{a\in\ships}\sum_{C\in\cycles^a}\Cost_C\bar{x}_C\}$
        \State $P_D\gets P_D\cup\{(\bar{\lambda},\bar{\omega})\}$
      \EndWhile
    \end{algorithmic}
  \end{algorithm}

  This algorithm is used in two phases.
  \begin{description}
    \item[Phase I] Solve (RLPBMP) as stated above.

      (RLPBMP) is solved using column generation.
    \item[Phase II] Solve restricted Benders master problem (RBMP) \emph{with} integrality constraint.

      This is done using branch-and-bound.
      To be efficient, branching is stopped when a small optimality gap is reached.
      
      No new columns are generated.
  \end{description}
\end{frame}

\begin{frame}[allowframebreaks]{Column generation for (RLPBMP)}
  (RLPBMP) is solved using column generation.
  We define dual variables
  \begin{itemize}
    \item $\Pi_{\lambda,\omega}$
    \item $\sigma^a$
  \end{itemize}
  corresponding to the two constraints of (BMP).
  
  The pricing problem for $a\in\ships$ then reads
  \begin{align*}
    \min_{C\in\cycles^a}-\left(\sum_{(\lambda,\omega)\in P_D}\left(\Cost_C-\sum_{e\in C\cap E_v}T^a\lambda_e\right)\cdot\Pi_{\lambda,\omega}+L_C\sigma^a\right)
  \end{align*}
  
  This can again be solved by searching for negative cost cycles in an auxiliary networks $G^a$, $a\in\ships$:
  \begin{align*}
    G^a&=(V,E^a)\quad\text{with }E^a=E_v^a\cup E_g^a \\
    c_v'&=\left(\sum_{(\lambda,\omega)\in P_D}\Pi_{\lambda,\omega}\right)c_v^a \\
    c_e'&=\frac{l_e}{7}\sigma^a
    -\left(\sum_{(\lambda,\omega)\in P_D}\Pi_{\lambda,\omega}\lambda_e\right)T^a
    +\left(\sum_{(\lambda,\omega)\in P_D}\Pi_{\lambda,\omega}\right)c_e^a
  \end{align*}
\end{frame}

\begin{frame}{Algorithmic refinements}
  \begin{enumerate}
    \item Add initial cuts to $P_D$.
    \item In phase II, stop solving (RBMP), which has \alert{integer constraints}, when an appropriate optimality gap is reached.
    \item Limit the number of iterations in phase II.
    \item Remove cycles that are no longer needed, i.e. have very low reduced costs.
    \item Restrict feasible cycles by length, number of visited regions, etc.
    \item Find negative cost cycles using dynamic programming and the following lemma.
  \end{enumerate}

  \begin{lemma}<2->
    For a negative cost cycle $v_1-v_2-\dots-v_n-v_1$,
    there is a node $v_k$ such that the following are all negative cost paths (indices modulo $n$):
    \begin{itemize}
      \item $v_k-v_{k+1}$
      \item $v_k-v_{k+1}-v_{k+2}$
      \item $v_k-v_{k+1}-v_{k+2}-\dots$
    \end{itemize}
  \end{lemma}
\end{frame}

\section{Computational experiments}

\begin{frame}{Comparison between algorithms}
  \begin{table}
    \resizebox{115mm}{!}{\input{comparison.pdf_tex}}
    \caption{Comparison between different algorithms}
  \end{table}
  \begin{description}\small
    \item[G] greedy algorithm
    \item[C] column generation-based algorithm
    \item[B] Benders decomposition-based algorithm
    \item[F] cycle generation based on flow decomposition
    \item[I] cycle generation based on iterative search algorithm
  \end{description}
\end{frame}

\begin{frame}{Analysis of Benders decompostion}
  \begin{table}
    \resizebox{110mm}{!}{\input{analysis_benders.pdf_tex}}
    \caption{Analysis of the Benders decomposition-based algorithm
    (last column: gap between the calculated integer solution and the LP relaxation)}
  \end{table}
\end{frame}

\begin{frame}{Effect of refinements}
  \begin{table}
    \resizebox{115mm}{!}{\input{effect_refinements.pdf_tex}}
    \caption{Effect of algorithmic refinements}
  \end{table}
\end{frame}

\metroset{sectionpage=none}
\section*{Questions?}
\metroset{sectionpage=progressbar}

\begin{frame}[standout]
  Questions?
\end{frame}

\appendix

\begin{frame}{Example network}
  \begin{figure}
    \centering{
      \resizebox{75mm}{!}{\input{network.pdf_tex}}
      \caption{Network with four ports and two cycles \cite{Agarwal2008}}
    }
  \end{figure}
\end{frame}

\againframe<2>{ssscr}

\againframe<2>{hardness}

\againframe<2>{greedy}

\begin{frame}{Effect of identical ships}
  \begin{table}
    \resizebox{115mm}{!}{\input{effect_identical.pdf_tex}}
    \caption{Effect of identical ships in the fleet}
  \end{table}
\end{frame}

\begin{frame}{Analysis of solutions}
  \begin{table}
    \resizebox{115mm}{!}{\input{analysis_solutions.pdf_tex}}
    \caption{Analysis of the obtained solutions}
  \end{table}
\end{frame}

\metroset{sectionpage=none}
\section{References}
\metroset{sectionpage=progressbar}

\begin{frame}[allowframebreaks]{References}
  \bibliography{bibliography}
  \nocite{*}
  \bibliographystyle{alpha}
\end{frame}

\end{document}
