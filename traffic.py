#!/usr/bin/env python3

import csv

with open('API_IS.SHP.GOOD.TU_DS2_en_csv_v2.csv', newline='') as file:
    reader = csv.reader(file)
    for row in reader:
        if len(row) >= 2:
            if row[0] == 'Country Name':
                title = row
            elif row[1] == 'WLD':
                world = row

with open('traffic.csv', 'w') as file:
    writer = csv.writer(file, delimiter=' ')
    writer.writerow(['Year', 'TEU'])
    for i in range(len(title)):
        if i >= 4 and world[i] != '':
            writer.writerow([title[i], float(world[i])/1000000])
